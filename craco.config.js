const CracoLessPlugin = require('craco-less');

const themeConfig = require('./src/styles/theme');

const modifyVars = Object.keys(themeConfig).reduce(
  (carry, key) => ({
    ...carry,
    [key.toLowerCase().replace(/_/g, '-')]: themeConfig[key],
  }),
  {},
);

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars,
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
