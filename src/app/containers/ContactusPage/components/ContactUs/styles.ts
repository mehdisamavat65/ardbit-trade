import styled from 'styled-components/macro';

export const StyledContactUs = styled.section`
  font-size: 1.3em;
  padding: 1.5em;

  .contactUsColInfo {
    margin-top: 0.8em;

    > span {
      margin-left: 0.3em;
    }
  }

  .map {
    height: 76vh;
  }

  .mapImg {
    margin-top: 0.3em;
    height: 100%;
    object-fit: fill;
    width: 100%;
  }
`;
