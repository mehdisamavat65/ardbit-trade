import styled from 'styled-components/macro';

export const StyledAddNewsModal = styled.section`
  margin-bottom: 1.5em;
`;
