import styled from 'styled-components/macro';

export const StyledTradingList = styled.section`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;
