import { Modal } from 'antd';
import styled from 'styled-components/macro';

export const StyledImportUsersFromExcel = styled.section``;

export const StyledModal = styled(Modal)`
  .description {
    white-space: pre-line;
  }
`;
