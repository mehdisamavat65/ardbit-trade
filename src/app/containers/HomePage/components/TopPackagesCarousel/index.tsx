/**
 *
 * TopPackagesCarousel
 *
 */
import { LeftCircleFilled, RightCircleFilled } from '@ant-design/icons';
// import { useWindowSize } from '../../../utils/hooks/useWindowSize';
import { useWindowWidth } from '@react-hook/window-size';
import { Button, Carousel, Divider } from 'antd';
import { TradeType, TopPackage } from 'app/containers/App/types';
import { translations } from 'locales/i18n';
import React, { memo, useCallback, useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { sizes } from 'styles/media';
import {
  StyledCarousel,
  StyledCarouselItem,
  StyledTopPackagesCarousel,
} from './styles';

interface Props {
  className?: string;
}

export const TopPackagesCarousel = memo(({ className }: Props) => {
  const { t } = useTranslation();

  const onlyWidth = useWindowWidth();
  const slidesToShow = useMemo(() => (onlyWidth > sizes.medium ? 6 : 2), [
    onlyWidth,
  ]);

  const items: Array<TopPackage> = [
    {
      id: 1,
      title: ' سهام 1',
      type: TradeType.ardbit,
      count: 2,
      price: 1500,
      totalPrice: 3000,
    },
    {
      id: 2,
      title: ' سهام 2',
      type: TradeType.others,
      count: 2,
      price: 500,
      totalPrice: 1000,
    },
    {
      id: 3,
      title: ' سهام 3',
      type: TradeType.ardbit,
      count: 4,
      price: 1000,
      totalPrice: 4000,
    },
    {
      id: 4,
      title: ' سهام 4',
      type: TradeType.ardbit,
      count: 2,
      price: 500,
      totalPrice: 1000,
      description: 'توضیحات در مورد سهام',
    },
    {
      id: 5,
      title: ' سهام 5',
      type: TradeType.others,
      count: 3,
      price: 500,
      totalPrice: 1500,
      description: 'توضیحات در مورد سهام',
    },
    {
      id: 6,
      title: ' سهام 6',
      type: TradeType.ardbit,
      count: 3,
      price: 1000,
      totalPrice: 3000,
    },
    {
      id: 7,
      title: ' سهام 7',
      type: TradeType.ardbit,
      count: 5,
      price: 1000,
      totalPrice: 5000,
      description: 'توضیحات در مورد سهام اردبیت',
    },
  ];
  const carouselRef = useRef<Carousel | null>(null);
  const handelPrevItem = useCallback(() => {
    if (carouselRef.current) {
      carouselRef.current.prev();
    }
  }, []);

  const handelNextItem = useCallback(() => {
    if (carouselRef.current) {
      carouselRef.current.next();
    }
  }, []);

  return (
    <>
      <StyledTopPackagesCarousel
        className={`TopPackagesCarousel ${className || ''}`}
      >
        <Divider className="divider">
          {t(translations.components.TopPackagesCarousel.title)}
        </Divider>
        <LeftCircleFilled className="leftItem" onClick={handelPrevItem} />
        <RightCircleFilled className="rightItem" onClick={handelNextItem} />

        <StyledCarousel
          dots={false}
          slidesToShow={slidesToShow}
          ref={carouselRef}
        >
          {items.map(item => (
            <StyledCarouselItem hoverable>
              <h3>{item.title}</h3>
              <div className="label">
                <b>{t(translations.global.TradeItem.currency_id)}</b>
                {t(translations.global.TradeType[item.type])}
              </div>
              <div className="label">
                {' '}
                <b>{t(translations.global.TradeItem.count)}</b>
                {item.count}
              </div>
              <div className="label">
                {' '}
                <b>{t(translations.global.TradeItem.price)}</b>
                {item.price}
              </div>
              <div className="label">
                {' '}
                <b>{t(translations.global.TradeItem.totalPrice)}</b>
                {item.totalPrice}
              </div>
              <div className="label">
                {' '}
                <b>{t(translations.global.TradeItem.description)}</b>
                {item.description}
              </div>

              <Button className="btn btn-secondary" block>
                {t(translations.global.TradeItem.btnBuy)}
              </Button>
            </StyledCarouselItem>
          ))}
        </StyledCarousel>
      </StyledTopPackagesCarousel>
    </>
  );
});
