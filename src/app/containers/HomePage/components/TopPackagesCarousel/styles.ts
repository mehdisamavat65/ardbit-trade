import { Card, Carousel } from 'antd';
import styled from 'styled-components/macro';
import theme from 'styles/theme';

export const StyledTopPackagesCarousel = styled.section`
  position: relative;
  padding: 5em 0 7em 0;
  margin-bottom: 1em;
  margin-top: 8em;
  background: #1d2e3f;
  .leftItem,
  .rightItem {
    position: absolute;
    font-size: 3em;
    top: 60%;
    transform: translateY(-50%);
    z-index: 1;
    color: #fff;
    opacity: 0.2;
    cursor: pointer;
    transition: opacity 200ms ease;
    filter: drop-shadow(0px 0px 2px black);

    &.leftItem {
      left: 0.25em;
    }

    &.rightItem {
      right: 0.25em;
    }

    :hover {
      opacity: 1;
    }
  }
  .divider {
    border: 1px solid ${theme.PRIMERY_COLOR};
    border-radius: 6em;
    margin-bottom: 0.2em;
    color: ${theme.TEXT_COLOR_INVERT};
    font-size: 2em;
  }
`;

export const StyledCarousel = styled(Carousel)`
  .slick-slide {
    text-align: center;
    height: 18em;
    line-height: 10em;
    background: ${theme.LIGHT_COLOR};
    overflow: hidden;
    padding: 0.5em 0;

    > div {
      height: 100%;
      display: flex;
      justify-content: center;
    }
  }
`;

export const StyledCarouselItem = styled(Card)`
  max-width: 200px !important;
  height: 100%;
  position: relative;
  border: 1px solid #eee !important;
  box-shadow: 0px 3px 5px #aaa !important;
  > button {
    z-index: 1;
  }
  .ant-card-body {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    padding: 0.5em;
    color: ${theme.TEXT_COLOR};

    .label {
      display: flex;
      flex-direction: row-reverse;
      justify-content: space-between;
      align-items: center;
      flex: 1;
      width: 100%;
      font-size: 1.2em;

      b {
        font-size: 0.8em;
      }
    }
  }
`;
