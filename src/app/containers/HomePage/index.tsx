/**
 *
 * HomePage
 *
 */

import { Col, Divider, Row } from 'antd';
import { ArdbitChangeHistoryChart } from 'app/components/ArdbitChangeHistoryChart';
import { ArdbitExchangeHisrotyChart } from 'app/components/ArdbitExchangeHisrotyChart';
import { FloatContactUs } from 'app/components/FloatContactUs';
import { InfoCarousel } from 'app/components/InfoCarousel';
import { TopPackagesCarousel } from 'app/containers/HomePage/components/TopPackagesCarousel';
import { translations } from 'locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { NewsCards } from './components/NewsCards';
import { homePageSaga } from './saga';
import { reducer, sliceKey } from './slice';
import { StyledHomePage } from './styles';
interface Props {
  className?: string;
}

export function HomePage({ className }: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: homePageSaga });
  const { t } = useTranslation();

  return (
    <StyledHomePage
      className={`HomePage ${className || ''}`}
      title={t(translations.pages.HomePage.title)}
      description={t(translations.pages.HomePage.description)}
    >
      <InfoCarousel />
      <NewsCards />
      {/* <TopPackagesCarousel /> */}
      <Row>
        <Divider className="dividerChart">
          {t(translations.pages.HomePage.chartTitle)}
        </Divider>
      </Row>
      <Row className="chartRow" gutter={16}>
        {/* <Divider className="dividerChart">
          {t(translations.pages.HomePage.chartTitle)}
        </Divider> */}
        <Col sm={24} md={12}>
          <ArdbitExchangeHisrotyChart />
        </Col>
        <Col sm={24} md={12}>
          <ArdbitChangeHistoryChart />
        </Col>
      </Row>

      <FloatContactUs />
    </StyledHomePage>
  );
}
