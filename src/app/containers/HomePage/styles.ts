import styled from 'styled-components/macro';
import { BaseLayout } from 'app/components/BaseLayout';
import theme from 'styles/theme';

export const StyledHomePage = styled(BaseLayout)`
  .dividerChart {
    border: 1px solid ${theme.PRIMERY_COLOR};
    border-radius: 6em;
    margin-top: 3em;
    margin-bottom: 0.2em;
    color: ${theme.TEXT_COLOR_INVERT};
    font-size: 1.5em;
    background: ${theme.DIVIDER_BACKGROUND};
  }

  .chartRow {
    margin: 4em 0 4em 0 !important;
  }

  .backTop {
    > div {
      height: 40;
      width: 40;
      line-height: '40px';
      border-radius: 4;
      background-color: '#1088e9';
      color: '#fff';
      text-align: 'center';
      font-size: 14;
    }
  }
`;
