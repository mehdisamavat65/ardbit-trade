import styled from 'styled-components/macro';
import { BaseLayout } from 'app/components/BaseLayout';

export const StyledFaqPage = styled(BaseLayout)`
  .ant-collapse-item {
    font-size: 1.3em;
  }
`;
