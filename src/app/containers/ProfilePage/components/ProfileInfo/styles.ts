import styled from 'styled-components/macro';

export const StyledProfileInfo = styled.section`
  .ant-descriptions-item {
    > span {
      font-size: 1.2em;
    }
  }
`;
