import styled from 'styled-components/macro';

export const StyledListTicket = styled.section`
  .ant-table-row-expand-icon.ant-table-row-expand-icon-collapsed {
    display: none;
  }
`;
