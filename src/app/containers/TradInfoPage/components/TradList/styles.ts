import styled from 'styled-components/macro';

export const StyledTradList = styled.section`
  .descriptionsItem {
    > span.ant-descriptions-item-label {
      font-size: 1.2em;
    }
    > span {
      font-size: 1.2em;
    }
  }
`;
