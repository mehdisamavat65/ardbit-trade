import styled from 'styled-components/macro';

export const StyledAddMessage = styled.section`
  margin-bottom: 1.5em;
`;
