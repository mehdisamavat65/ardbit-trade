import styled from 'styled-components/macro';
import theme from 'styles/theme';

export const StyledAddPackage = styled.section`
  margin-bottom: 1.5em;
`;

export const StyledCalculatePackage = styled.section`
  color: ${theme.CALCULATE_PACKAGE_COLOR};
  margin-top: -0.7em;
  margin-bottom: 0.7em;
  display: block;
`;
