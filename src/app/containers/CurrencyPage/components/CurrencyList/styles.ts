import styled from 'styled-components/macro';

export const StyledCurrencyList = styled.section`
  .currencyList {
    width: 50%;
    text-align: 'center';
  }
`;
