import { Select } from 'antd';
import styled from 'styled-components/macro';

export const StyledTradeTypeSelect = styled(Select)`
  min-width: 10em;
`;
