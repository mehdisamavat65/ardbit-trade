/**
 *
 * BaseLayout
 *
 */
import React, { memo, ReactNode } from 'react';
import { Helmet } from 'react-helmet-async';
import { Footer } from './components/Footer';
import { Navbar } from './components/Navbar';
import {
  StyledBaseLayout,
  StyledContent,
  StyledFooter,
  StyledHeader,
} from './styles';

interface Props {
  className?: string;
  children: ReactNode;
  title?: string;
  description?: string;
}

export const BaseLayout = memo(
  ({ className, children, title, description }: Props) => {
    return (
      <StyledBaseLayout className={`BaseLayout ${className || ''}`}>
        <Helmet>
          {title && <title>{title}</title>}
          {description && <meta name="description" content={description} />}
        </Helmet>

        <StyledHeader>
          <Navbar />
        </StyledHeader>

        <StyledContent>{children}</StyledContent>

        <StyledFooter>
          {' '}
          <Footer />
        </StyledFooter>
      </StyledBaseLayout>
    );
  },
);
