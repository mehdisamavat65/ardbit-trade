import { Layout } from 'antd';
import styled from 'styled-components/macro';
import theme from 'styles/theme';

export const StyledBaseLayout = styled(Layout)``;

export const StyledHeader = styled(Layout.Header)`
  background: ${theme.HEADER_BACKGROUND_COLOR};
  line-height: 79px;
  height: 80px;
`;
export const StyledContent = styled(Layout.Content)`
  min-height: 100vh;
`;

export const StyledFooter = styled(Layout.Footer)`
  background: ${theme.LAYOUT_FOOTER_BACKGROUND};
`;
