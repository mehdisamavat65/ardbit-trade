import { Carousel } from 'antd';
import styled from 'styled-components/macro';
import { media } from 'styles/media';
import theme from 'styles/theme';
import { Nullable } from 'types';

export const StyledInfoCarousel = styled.section``;

export const StyledCarousel = styled(Carousel)`
  .slick-slide {
    text-align: center;
    height: 40em;
    line-height: 10em;
    background: ${theme.LAYOUT_HEADER_BACKGROUND};
    overflow: hidden;

    > div {
      height: 100%;
    }
  }

  ${media.medium`
    .slick-slide {
      height: 50vh;
    }
  `}
`;

export const StyledInfoItem = styled.div<{
  background: Nullable<string>;
  textColor: Nullable<string>;
}>`
  height: 100%;
  width: 100%;
  display: flex !important;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 2em;
  color: ${props => props.textColor || theme.TEXT_COLOR_INVERT};
  background-repeat: no-repeat !important;
  background-size: cover !important;
  background: ${props =>
    props.background?.lastIndexOf(':')
      ? `url(${props.background})`
      : props.background || 'transparent'};

  .title {
    color: inherit;
    line-height: 1.5em;
  }
`;
