/**
 *
 * InfoCarousel
 *
 */
import { Button } from 'antd';
import { GlobalInform } from 'app/containers/App/types';
import React, { memo } from 'react';
import { StyledCarousel, StyledInfoCarousel, StyledInfoItem } from './styles';
import three from 'app/components/LazyImg/assets/ardbit-pic6.jpeg';
import two from 'app/components/LazyImg/assets/ardbit-pic2.jpg';
import one from 'app/components/LazyImg/assets/ardbit-pic3.jpeg';
import four from 'app/components/LazyImg/assets/ardbit-pic4.jpg';

interface Props {
  className?: string;
}
export const InfoCarousel = memo(({ className }: Props) => {
  const items: Array<GlobalInform> = [
    {
      id: 2,
      background: `${two}`,

      color: null,
      title: ' اردبیت بهترین بستر سرمایه گذاری ',
      link: { title: 'لینک به اردبیت', url: 'https://www.ardbit.net/' },
    },

    {
      id: 3,
      background: `${one}`,
      title: 'سود بالا و غیر قابل مقایسه با بورس',
      color: '#fff',
      link: null,
    },
    {
      id: 1,
      background: `${three}`,
      title: 'شما هم در تشکیل یک استارت اپ بزرگ شریک باشید',
      color: null,
      link: null,
    },
    {
      id: 4,
      background: `${four}`,

      color: null,
      title: 'حتی با سرمایه اندک هم میتوانید در یک سود بزرگ شریک باشید',
      link: null,
    },
  ];

  return (
    <StyledInfoCarousel className={`InfoCarousel ${className || ''}`}>
      <StyledCarousel autoplay>
        {items.map(item => (
          <StyledInfoItem
            key={item.id}
            textColor={item.color}
            background={item.background}
          >
            {item.title && <h3 className="title">{item.title}</h3>}
            {item.link && (
              <Button
                className="btn btn-secondary"
                href={item.link.url}
                target="_blank"
              >
                {item.link.title}
              </Button>
            )}
          </StyledInfoItem>
        ))}
      </StyledCarousel>
    </StyledInfoCarousel>
  );
});
