import styled from 'styled-components/macro';
import theme from 'styles/theme';

export const StyledUserNotifiction = styled.section`
  margin-left: -0.8em;
  .ant-btn-link {
    color: ${theme.TEXT_COLOR_INVERT};
  }
  .test {
    background: red;
  }
`;
